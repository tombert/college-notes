#!/bin/bash
pandoc --toc -s discrete_mathematics.md -o discrete_mathematics.pdf --filter ./tikz.py -H deeplist.tex
