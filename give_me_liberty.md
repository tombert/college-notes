---
title:  "Give Me Liberty Notes"
---


# Chapter 15: What is Freedom? 

## Evening, January 12, 1865
  - Black leaders meet with Sherman and Stanton for a discussion
  - Several of these men would later have prominent positions
    - Ulysses S. Houston
      - Pastor of the Third African Baptist Church
    - James Porter
      - An Episcopal leader
      - Operated a secret school for black children before the war. 
      - Later won an election for the Georgia legislature
    - James D. Lynch
      - Born free
      - Became the secretary of state of Mississippi
  - This conversation was instrumental in that it showed black leaders coming out of slavery a definition of freedom. 
  - Garrison Frazier
    - What is slavery?
      - "Receiving by irresistible power the work of another man". 
    - What is Freedom? 
      - "Placing us so that we could rerap fruit of our own labor".
    - How would we know we accomplished Freedom? 
      - Have land, and turn it and till it by ones own labor
    - Garrison Frazier believed blacks had sufficient intelligence to handle freedom. 
  - This meeting foreshadowed radical changes
    - These changes would come to be known as "Reconstruction" 
      - This literally means "rebuilding a shattered nation" 

## Years Following Civil War. 
  - In the years after the civil war, we would have to start redefining our meaning of the term "American Freedom". 
  - Freedoms that the whites were entitled expanded to the blacks. 
  - The Constitution would have to be rewritten to accommodate blacks
    - This included counting them as citizens. 
  - A wave of black schools started to flourish. 
  - Black women started going to work at a higher frequency than white women. 

## Church and School
### Church
  - After the war, blacks largely left white churches and made churches of their own. 
    - Before the civil war, roughly 42,000 blacks were worshipping in the Methodist church. 
    - After the war, only around 600 remained. 
  - Churches redrew the map of the south by housing: 
    - Schools.
    - Social events.
    - Political gatherings.
      - Ministers would routinely play large roles in politics.

### School
  - Freed people wanted education in droves. 
    - Frederic Douglas: Education is "the next best thing to liberty". 
    - The huge desire was driven by: 
      - A desire to read the bible
      - Preparation for the marketplace
      - Taking part in politics.
  - Black people of all ages flocked to schools. 
    - Most of these schools were established by ex-slaves. 
  - Sidney Andrews
    - Reporter from the North. 
      - Greatly impressed by blacks educating themselves outside of the classroom.
        - Noticed that black children and adults would routinely read books, even when they were not required for the class. 

  - The success of elementary schools led to the establishment of black colleges and universities. 
    - Several examples include: 
      - Fisk University
      - Hampton Institute 
      - Howard University

## Political Freedom
  - A priority in the zeitgeist of black Americans was the right to vote
    - Frederic Douglas: "Slavery is not abolished until black man has ballot"
    - By denying people a right to vote, a stigma of inferiority followed.
    - A dissatisfaction with the current state of affairs for blacks led to a large black presence in the public sphere with events like: 
      - Conventions
      - Parades
      - Petition drives
  - Anything less than full citizenship felt like a betrayal of democracy.

## Land Labor and Freedom
  - A theme that felt central to freedom was owning land. 
    - Owning land would enable the poor class to enjoy the "sweet boon of freedom". 
    - By owning land, blacks could develop independent communities free of white control. 
  - Many former slaves argued that by contributing years of unpaid labor, they acquired rights to the land
    - This would sometimes result in former slaves "seizing property", and sleeping in rooms in their former masters' house.  

## Masters without slaves. 
  - After the war, many white Southerners were dismayed.  
    - A lot of the land is destroyed. 
    - There was a lot of dissatisfaction by submitting to Northern demands.
  - Around 260,000 men were killed for the Confederacy during the war. 
    - This was a staggering one-fifth of the entire male population of the South. 
  - The economic revival was slow and painful. 
    - Many people lost their life savings by investing heavily into now-worthless confederate bonds. 
    - A lot of people had never done any physical labor before. 
  - Southern Planters. 
    - Many Southern planters took a very narrow definition of "freedom"
      - There seemed to be some difficulty in understanding that freedom means equality. 
      - They wanted to feel that they still exercise a lot of control. 

## The Free Labor Vision. 
  - The Republican north tried to implement a system to best use the new-found free labor. 
    - It was observed that a free market workers worked harder than slaves. 
  - Planters sought to create a system that was closer to slavery. 

## The Freedman's Bureau
  - O.O. Howard.
    - A veteran of Civil War. 
    - In charge of creating the bureau. 
  - Bureau responsibilities: 
    - Establishing schools. 
    - Provide aid.
    - Settle disputes between blacks and whites. 
  - Only lasted from 1865 to 1870
    - The service was never taken especially seriously, and there were never more than a thousand agents in the south. 
    - The bureau, while not terribly successful, did do a bit of good. 
      - They helped coordinate and finance the establishment of schools. 
        - Led to three-thousand schools, and over one-hundred-fifty-thousand pupils. 
      - Healthcare
        - The bureau made sure to expand hospitals to the new black communities. 
  - Did more harm than good for economics. 

## Failure of Land Reform
  - The bureau was allowed to divide abandoned and confiscated land amongst freed men. 
    - Andrew Johnson later ordered that all land be returned to the previous owners. 
  - A vast majority of rural areas stayed poor. 
    - Many slaves, in order to make any money, were forced to work on the same plantations and bossed by their former owners. 
      - Due to the low payment, this effectively created wage-slaves. 

## Towards a New South. 
  - Task System. 
    - In a task system, workers are assigned daily tasks
      - Upon completion of these tasks, they were allowed to leave. 
        - This mode of work dominated the Rice Kingdom. 
  - Sharecropping
    - This started as a compromise between blacks' desire for land ownership, and planters demand for discipline. 
    - Allowed family to rent part of the plantation. 
      - Crops were then divided between the owner and worker at the end of the year. 
    - Planters loved this because it gave a guaranteed labor force
      - Over time, the landowners became increasingly more brutal and oppressive. 

## The White Farmer
  - Small farmers.
    - Traditionally, small farmers would mostly just grow food for their family. 
    - After the war, a lot of the small farmers' property was destroyed. 
      - In order to make ends' meet, small farmers were forced to take up growing cotton. 
        - Most of these farmers would pledge some of their crops as collateral. 
    - Many farmers were still in debt. 
    - More blacks than whites rented land. 

## Urban South. 
  - Unlike the rural areas, the urban South grew very quickly. 
    - This is largely due to the advent of the railroad, which in turned allowed merchants in Atlanta to trade with the North quickly and easily. 

## Aftermath of Slavery
  - The United States was not the only place to have struggles after slavery. 
    - In basically every case, planters tried very hard to encourage or require former slaves to go back to their plantation. 
    - The stereotype that persisted was that slaves were lazy and thought "freedom" meant "freedom from work". 
  - Many former slaves tried to carve out vessels of independence by withdrawing women and children from field labor. 
  - Southern planters starting bringing in Chinese workers to replace blacks. 
  - The unique part of the way the United States handled slavery is that within two years of freedom, blacks were allowed to vote. 
    - This fact led to a long battle between Johnson and Congress. 


## Making of Radical Reconstruction. 
  - Andrew Johnson
    - In charge of overseeing restoration of the union. 
    - Born poor, and became a tailor's apprentice. 
    - Became a governor of Tennessee. 
    - Became a champion of the "honest yeoman". 
    - Took criticism very poorly. 
      - As a consequence, he was very difficult to compromise with. 
    - Believed that since the secession was illegal in the first place, the south never sacrificed their states' rights. 
    - Felt very firmly that blacks had no role in reconstruction. 

## Failure of Presidential Reconstruction. 
  - May 1865
    - Johnson shows plan for reuniting nation. 
      - In this plan Johnson offers a pardon to southerners who swore allegiance. 
        - This was not the case if you were worth more than $20,000
      - Absolved south of much of its debt. 
      - The Northern Republicans were open-minded initially. 
        - Turned against him once observing the Southern conduct. 

## Black Codes. 
  - The Black Codes were laws passed by the Southern governments that tried to heavily regulate the lives of former slaves. 
    - These would commonly deny former slaves rights like: 
      - Being allowed to testify against white men, 
      - Serve on juries
      - Vote. 
      - Buying land. 
    - Some other bizarre rules were: 
      - Requiring signing yearly labor contracts
        - Failure to sign could be arrested and hired out. 
      - Forcing black children to work, regardless of parents' permission. 

## The Northern Response to the Black Codes. 
  - Many Northerners felt that the Black Codes basically just re-legalized slavery. 
  - North was unhappy. 
    - Many Northerners felt that the Confederates were treated to leniently 
      - Southern leaders would be arrested and subesquently released
        - Only Henry Wirz was executed 
          - Commander of Andersonville Prison. 
      - They weren't mad because they wanted more punishment, but more of an acceptance from them. 

## Radical Republicans
  - Johnson prematurely announces that the nation was fully reunited. 
  - A group of Radical Republicans wanted the dissolution of the Southern governments.
    - The Radicals felt that the "rebels" were too empowered in the South.
    - Wanted a guaranteed right for black men to vote. 
  - Most of these radicals tended to be from the "burned over" districts of the rural North. 
  - The Radicals fully embraced the federal government's power during the Civil War. 
    - They held the belief that states' rights shouldn't get in the way of your human rights. 
  - The most prominent of these people were Sumner and Stevens. 
    - They encouraged Lincoln to free the slaves. 
    - They also wanted to confiscate the land of disloyal planters and divide it amongst the slaves. 

## Origins of Civil Rights. 
  - Republicans had a majority in Congress. 
    - This was good for them, but there was still a divide, since most of them were moderates, not Radical. 
    - Many believed that Johnson's plan was flawed, but they would rather try and fix it than come up with a new one. 
  - Civil Rights Bill of 1866
    - The bill defined all people born in the United States as citizens. 
      - These people would be given inalienable rights. 
        - This effectively removed the Black Codes. 
        - No state could take away your rights to: 
          - Form Contracts
          - Sue
          - Enjoy equal protection from the police. 
    - Johnson vetoed the bill
      - He claimed that it expanded the power of the national government, therefore reducing states' rights
      - This made a conflict with Republicans inevitable
      - This became the first bill to override a presidential veto. 

## Fourteenth Amendment. 
  - Congress started its own plan for reconstruction. 
    - They quickly approved the 14th Amendment and sent it off to states. 
      - Granted the rights of the Civil Rights bill permanently. 
      - Still didn't give black people the right to vote. 
        - A compromise was met: if they denied the vote to blacks, they would get lower representation in congress
          - This way they had a choice: allow them to vote, or don't be represented. 
    - Introduced division
      - No democrats signed at all. 
      - The Radicals were also unhappy due to the lack of voting rights. 

## Reconstruction Act. 
- The 14th Amendment became a big campaign issue for Johnson
  - Using it, he tried to swing voters to support his Reconstruction plan. 
  - Claimed that the Radical Republicans were trying to assassinate him. 
  - Every Southern state but Tennessee refused to ratify the 14th. 
    - This pushed more moderates to the "radical" side. 
- A new Reconstruction Act was introduced. 
  - This divided the South into districts
  - It called for creation of new state governments
  - Didn't do much to end conflict with Johnson. 

## Impeachment and Election of Grant
- Tenure of Office Act
  - Said that the president couldn't remove office holders without Congress's approval. 
    - Johnson said that this was illegal, so he removed Stanton (Secretary of War)
      - Due to this flagrant violation of law, Congress impeached. 
- Impeachment
  - Johnson's impeachment was the first in U.S. history. 
  - He was placed on trial for "High Crimes and Misdemeanors" 
  - Republicans saw him as an utter failure, but some felt that his replacement (Benjamin Fwade) was even worse. 
  - Johnson's lawyer said that he would stop interfering with reconstruction if he was acquitted. 
  - The vote was 35-19 to impeach. 
    - It was only one vote short. 
- Republicans nominate Ulysses S. Grant

## Fifteenth Amendment
  - This fully allowed people to vote, regardless of race. 
    - People would find loopholes, but this is a step in the right direction. 

## Boundaries of Freedom. 
  - Reconstruction redrew the boundaries of freedom
  - A treaty with China (the Burlingame Treaty) was reached. 
    - Frederic Douglas condemned prejudice against Chine immigrants
  - Asians still could not become citizens. 

## Rights of Women
  - Women encountered more limits in Reconstruction
  - Feminists saw Reconstruction as a good opportunity to claim their own rights. 
    - As a result, they started to rise up against inequality and unequal pay.
    - They also fought for the liberation of divorce laws and birth control. 

## Feminists and Radicals
  - Feminists sadly didn't find a lot of male listeners, even with the Radicals. 
  - This, unfortunately, forced people like Susan B Anthony to try and appeal to the racists
    - Some feminists would argue that white women were obviously more valuable than any non-white man. 


## Tocsin of Freedom
  - Africans ask for exactly the same rights as whites.
  - Strikes would organized for higher wants.
  - Many blacks joined the "union league", which allowed a vast majority of blacks to vote. 

## Black Office Holder
  - Only South Carolina had a black majority in office. 
  - Slowly, other black people started to get elected. 


## Carpet Baggers and Scalawags. 
  - "Carpetbagger" 
    - Northerners who made home in the South after the war. 
  - "Scalawags" 
    - Traitors to race and region.

## Reconstruction Opponents 
  - Traditional Southern leaders didn't like the new government 
    - Claimed it supported Black Supremacy. 

## Reign of Terror
  - After the war, many blacks were assault and murdered 
    - This was largely due to unwillingness to submit to white for everything, such as getting off the sidewalk when a white person was walking by. 
  - Ku Klux Klan
    - The KKK Served as a military arm for the democrats of the South. 
    - From the beginning, the KKK was a terrorist organization. 
    - Victims: 
      - White Republicans
      - Teachers
      - Party Organizers
      - William Luke
        - Irish school teacher who was lynched.
    - At some points, the KKK would escalate to mass terrorism. 
      - Meridian, Mississippi
        - 30 blacks were murdered on the street in cold blood. 
      - Colfax Louisiana
        - Hundreds of former slaves were murdered. 
      - Enforcement acts
        - Federal Government stepped in to help stop the terror. 

## Liberal Republicans
  - The North slowly stopped caring as much about Reconstruction. 
    - They started to feel like the South should start taking care of their own problems. 
  - A group of republicans claimed that the Northern Republicans were corrupt. 
    - They said that the north manipulated immigrants. 

## North Retreat
  - Liberal created a resurgence of racism in the North. 
        








