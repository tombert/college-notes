---
title:  "Voices Of Freedom Notes"
---

# Mississippi Black Codes
  - Andrew Johnson put reconstruction plans into effect in 1867
    - New governments elected by white voters would be placed into the South. 
    - Some of the first laws passed were the black codes. 
      - These codes actually did grant a few rights. 
        - Marriage.
        - Property.
        - Limited access to the courts. 
      - They also denied a lot of fundamental freedoms. 
        - Blacks could only testify in cases where bother black people were involved.
          - Blacks could *not* testify in cases that only involved white people.
        - Freed people could also not serve on juries.
        - Freedmen could not serve in state militias. 
        - Blacks still could not vote. 
      - Blacks were also forced to sign labor contracts
        - If they refused, they could be arrested and possibly hired out via court-order.
      - These codes showed what happens when the south was allowed to self-regulate.
        - This worked really well to discredit Johnson. 

## Vagrant Law
  - If any black person failed to pay a tax (specific to a black person): 
    - This would be considered evidence of vagrancy. 
    - As a consequence, a sheriff ma arrest that person. 

## Civil Rights Of Freeman.
  - A freed person can sue or be sued. 
    - They could also own property
    - This could only be done in incorporated cities or towns. 
  - Freedmen could now marry within their race.
    - Freed people who identified as husband and wife before are now legally married.
    - It is illegal to marry between different races.
      - A white person cannot marry anyone less than three generations removed from black person.
  - All freed men must have written evidence of lawful employment or residence.
  - Any contract with a black person longer than a month must have it in writing. 
  - Any officer can force any black man back to his previous employer. 

## Certain Offenses of Freedman
  - Blacks must not carry firearms
    - This is punishable with a fine. 
  - Any black accused of: 
    - Riots. 
    - Routs.
    - Affrays.
    - Trespassing.
    - Malicious mischief.
    - Cruelty to animals.
    - Seditious speeches.
    - Insulting gestures or language.
    - Assault.
    - Being a minister without a license. 
    - TODO: NEED TO FIND PUNISHMENT FORGOT TO WRITE DOWN.
  - If a white person gives black person any booze or weapons weapons will be will be fined up to $50.
  - If a black person is convicted of a crime and doesn't pay a fine, he may be hired out by a judge.

## Questions.
  - Why do you think that the state of Mississippi required all black citizens to sign labor contracts, but not white citizens? 
    - I am assuming that many of the higher-ups saw the labor contracts as a "loophole" to the abolition of slavery.  Sure, they couldn't "own" them anymore on paper, but they could still force them to work for them. The fact that whites were not forced to sign these contracts probably stems from a mentality that blacks are "inferior" to white people.  

  - What basic rights were guaranteed to the former slaves, and which are denied to them by the Black Code? 
    - The rights that are guaranteed to slaves: 
      - To sue or be sued. 
      - Owning property. 
    - The rights that were denied: 
      - Voting.
      - Owning land outside fo approved areas. 
      - Owning guns or booze.
      - Testifying in cases that involve both a white plaintiff and defendent.
      - Marriage between the races. 
      - Freedom from particular employment. 
        - What I mean is that if they refused to sign a labor contract, they could be arrested, and forcibly hired out. 

# A Sharecropping Contract
  - For a variety of reasons, many former slaves had trouble acquiring land. 
    - As a consequence of this, many ended up signing sharecropping contracts. 
    - This practice started in Shelby, Tennessee. 

  - Laborers would typically sign the contracts with an "X". 
    - This is a common signature for illiterate people.
  - The contracts would usually give the planter the right to supervise the labor of the employees. 
  - Later sharecropping contracts would give former slaves more autonomy. 
  - How this worked: 
    - Families would rent parcels of land
    - These families would work under their own direction. 
    - Afterwards they would divide the crop with the owner at the end of the year. 
  - After cotton prices began to decrease, it became more difficult for them to make any profit. 

## Thomas J. Ross. 
  - Ross agreed to employ Freedmen to plant and raise crops on his Rosstown Plantation. 
  - In doing this, he agreed to: 
    - Furnish land to cultivate. 
    - Provide a sufficient amount of mules and horses and keep them fed. 
    - Provide housing for the crops. 
    - Provide all the necessary tools to till the land. 
  - The Freedman and Ross split the profits in half for: 
    - Cotton
    - Corn
    - Wheat
  - This split was *after* Ross did deductions for expenses. 
  - The Freedman agreed to furnish his own living materials:
    - Clothing.
    - Medicine.
    - Family provisions. 
  - If Ross was forced to furnish any of these things, the price would be deducted from the Freedman's shares. 
    - Ross kept a book of all the accounting information. 
  - By accepting the contract, the Freedman agreed to ten hours of work a day. 
    - If the contract is canceled before official termination, the Freedman must pay one dollar a day for all the remaining days. 
  - By signing the contract, the Freedman agrees that he must obey all of Ross's orders. 
    - Ross reserved the right to dock for disobedience. 










