---
numbersections: true
title: Discrete Mathematics Notes
header-includes: | 
 \usepackage{tikz}
 \usepackage{graphicx}
 \usepackage{pgfplots} 
 \usepackage{amsmath}
 \usetikzlibrary{decorations.markings,arrows,shapes,positioning}

 \newenvironment{rcases}
    {\left.\begin{aligned}}
    {\end{aligned}\right\rbrace}

---

# Chapter 1
## Mathematics
  - It's important to state things precisely in mathematics
  - Every word is precise and necessary, and purposefully unambiguous
  - In order to understand it, you must master the art of logical thinking

## Formal Logic
  - Students are often scared when looking at the chalk board of logicians. 
    - There are often scary and bizarre symbols that are initially confusing. 
    - There are good reasons for these symbols.
      - English (and other spoken languages) can have ambiguous wordings and phrasings. 
      - Reducing a problem to the guts of it can often simplify the problem to the raw essence of it. 
      - These rules are simple and unambiguous
    - By using these symbols, you can often avoid worrying what all these symbols really mean. 
      - e.g. When you are doing multiplication, you typically do something like $524 \cdot 243$ by following an algorithm, and not simply adding 243 together 524 times. 
      - This algorithm is simple and well-defined for multiplication.
      - After the work is done, it's important to know how to *interpret* the rules. 

## Inquiry Problems
  - Example: 
    - Wesley says he is holding a quarter in his left hand, and twenty dollars in his right. 
      - You think that he's lying. 
      - How would you go about demonstrating that he's lying. 
    - Suppose Buttercup knows if Wesley is lying. 
      - She says that if he is lying, she will give you a cookie. 
        - What can you reasonably conclude if she gives you a cookie? 
        - What can you reasonably conclude if she does not give you a cookie? 
  - Another Example: 
    - There exist two camps: Camp Halcyon and Camp Placid. 
      - Camp Halcyon's policy
        - Two If you used the pool in the afternoon, and you did not clean up during lunch, you must clean up during dinner. 
      - Camp Placid Policy
        - You must choose any one of the following: 
          - Stay out of the pool in the afternoon. 
          - Clean up after lunch. 
          - Clean up after dinner. 
      - How do these policies differ, if they do at all? 

## Connectives and Primitives. 
  - We need a deterministic system for translating statements to symbols if we want to logically reason about them. 

### Statements
  - Statements are also known as propositions. 
  - It is simple a declaration of something true or false. 
    - It *cannot* be both if you want it to be a statement. 
    - This is called "the law of the excluded middle".
  - Examples of statements: 
    - $J$ is odd
    - $1+1=4$
    - If it is raining outside, the ground is wet. 
    - Our professor is from Mars. 
  - Notice that the statements themselves do not have to be true in order to be statements. 
  - Examples of things that *are not* statements. 
    - $x$ is even. 
      - We do not know what $x$ is. 
        - This is known as a free variable. 
      - This means we have no way of deducing the truth or falsity of the statement. 
    - This statement is false. 
      - "Liars Paradox"
      - Self referencial. 
      - Theres no reasonable way of figuring out if this statement is true or false, therefore it's not a statement. 

### Connectives
  - Complicated statements typically consist of multiple simple statements glued together. 
  - Example of slightly more complicated statement. 
    - If $p$ is "you are wearing shoes" and $q$ is "you cannot cut your toenails", you would structure it like: 
      - $p \rightarrow q$
      - This translates to "if you are wearing shoes, you cannot cut your toenails". 
      - Something like $\neg q$ would mean "it is not the case that you cannot cut your toenails". 
  - Connective Table

       | Name           | Symbol |
       |----------------|--------|
       | And            |$\land$ |
       | Or             |$\lor$  |
       | Not            |$\neg$  |
       | Implies        |$\rightarrow$|
       | If and only if |$\leftrightarrow$|

## Truth Tables
  - We still need to define meaning to the connectives. 
    - This is sometimes a bit harder than it may seem for something like English
    - Logical connectives tend to be a bit simpler. 
  - The "not" connective.
    
    |$p$| $\neg$ p |
    |---|----------| 
    | T | F        |
    | F | T        |

  - The "and" connective.

    |$p$|$q$| $p \land q$  | 
    |---|---|--------------|
    | T | T | T            |
    | F | T | F            |
    | T | F | F            |
    | F | F | F            |

    - For "and" to be true, both statements inside it must be true. 
  - The "or" connective. 
    
    |$p$|$q$| $p \lor q$ |
    |---|---|------------|
    | T | T | T          |
    | T | F | T          |
    | F | T | T          |
    | F | F | F          |

    - For "or" to be true, only one of the statements inside has to be true. 
  - If and only if (also known as iff) table: 
    
    |$p$|$q$| $p \leftrightarrow q$ |
    |---|---|-----------------------|
    | T | T |   T                   |
    | F | T |   F                   |
    | F | F |   T                   |
    | T | F | F                     | 

    - Only true if both of the assertions have the same value. 
  - If...then (also known as "implies) table: 

    | $p$ | $q$ | $p \rightarrow q$ |
    |-----|-----|-------------------|
    | T   | T   |   T               |
    | T   | F   |   F               |
    | F   | F   |   T               |
    | F   | T   |   T               |

    - Only false if the first part is true, and the antecedent is false. 
    - This one is slightly confusing, but think of it this way: 
      - "If you are wearing shoes, you cannot cut your toenails". 
        - In order to show falseness, you need to show that you can cut your toenails with shoes on. 
        - If you live in a world without shoes, the statement is vacuously true. 
          - By saying "if wearing shoes", we're basically saying anything is possible after that. 
    - Logical Equivalence
      - Statements are logically Equivalent if they share the same truth table. 
      - Example: This is true: 
        - If quadrilateral has a pair of parallel sides, it must have a pair of supplementary angles. 
          - We can write this in the form $p \rightarrow q$
            - $p=$ "Has a pair of parallel sides"
            - $q=$ "Has a pair of supplementary angles"
          - Contrapositive
            - Contrapositives take the form of $\neg q \rightarrow \neg p$
              - "If quadrilateral does not have supplementary sides, then it does not have parallel sides".  
              - This is valid logic and will always be true. 
          - Converse
            - $q \rightarrow p$ 
              - "If quadrilateral has pair of supplementary angles, then it has a pair of parallel sides". 
              - This does not logically follow, and gives no guarantee of truth. 
      - Converse
        - The converse of a statement is not always true. 
          - Example: If a company is not participating in illegal acts, then an audit will turn up no evidence of wrongdoing
            - $p =$ "Company participating in illegal acts"
            - $q =$ "audit turning up evidence of wrongdoing" 
            - $\neg p \rightarrow \neg q$
            - Converse would be $\neg q \rightarrow \neg p$
              - This translates to If an audit turns up no evidence of wrongdoing, the company is not participating in illegal acts. 
                - This does not necessarily follow; the audit might have missed something. 

## Example of large truth table
  - If Aaron is late, then Bill will be late, and if both Aaron and Bill are late, then class will be boring. 
    - $p=$ "Aaron is late" 
    - $q=$ "Bill is late"
    - $r=$ "Class is boring"
    - $S=\left( p \rightarrow q \right) \land \left[ \left(p \land q\right) \rightarrow r\right]$
    - Truth table: 

    |$p$|$q$|$r$|$p \rightarrow q$| $p \land q$ | $\left( p \land q \right) \rightarrow r$| S|
    |---|---|---|-----------------|-------------|-----------------------------------------|--|
    | T | T | T | T               | T           | T                                       | T|
    | T | T | F | T               | T           | F                                       | F|
    | T | F | T | F               | F           | T                                       | F|
    | T | F | F | F               | F           | T                                       | F|
    | F | T | T | T               | F           | T                                       | T|
    | F | T | F | T               | F           | T                                       | T|
    | F | F | T | T               | F           | T                                       | T|
    | F | F | F | T               | F           | T                                       | T|

## Propositional Logic
  - Truth Tables can be annoying
  - Each time you add a new statement, you end up having to double the number of rows
  - We need a nicer, more general system to do anything outside of little toys. 

### Propositional Calculus
  - Propositional Calculus allows us to make formal deductions. 
    - This is useful for analyzing complex problems. 
    - This is commonly used in mathematical discourse and serves as simple mathematical proofs. 

#### Tautologies and Contradictions

##### Tautologies
  - A tautology is a statement that will always return true no matter what. 
  - Example
    - $\left(p \land q\right) \rightarrow p$
  - $A \implies B$ means $A \rightarrow B$ is true in all cases. 
  - $A \iff B$ means $A \leftrightarrow B$ is true in all cases. 
  - When a tautology is in the form of $\left(C \land D\right) \implies E$, we right it as follows: 
    $$
    \begin{rcases}
      C \\
      D 
    \end{rcases}
    E
    $$
    - This highlights the fact that if you know C and D, you can conclude E. 
  - Example:  Prove
    $$
    \begin{rcases}
      p \\
      p \rightarrow q 
    \end{rcases}
    q
    $$

    |$p$|$q$|$p\rightarrow q$|$p\land\left(p\rightarrow q\right)$|$S$|
    |---|---|---------------|----------------------------------|--|
    | T | T |       T       | T                                | T|
    | T | F |  F            | F                                | T|
    | F | T | T             | F                                | T| 
    | F | F | T             | F                                | T|

      - This is called "modus ponens" 
    
##### Contingencies
  - A contingency is always false
    - $a \land \neg a$

## Derivation Rules
  - Tautologies turn out to be really useful
    - They can easily show how statements are deduced from another. 
    - Example: 
      - The professor does not own a spaceship
      - If the professor is from Mars, the professor must own a spaceship
      - Modus Tollens says therefore the professor is not from Mars. 
        - This shows it's a valid argument
        - We can reasonably conclude the last statement given the first two. 
  - Any tautology can be used to derive new statements. 
    - Equivalence Rules
      - Logical Equivalence. 
        
        | Equivalence                                                      | Name           | 
        |------------------------------------------------------------------|----------------|
        |$p \iff \neg\neg p$                                               | double negation|
        |$p\rightarrow q \iff \neg p \lor q$                               | implication    | 
        |$\neg \left(p\land q\right)\iff \neg p \lor \neg q$               | De Morgan's Law|
        | $p\land q \iff q \land p$                                        | Commmutativity |
        | $p \land \left(q\land r\right) \iff \left(p\land q\right)\land r$| Associativity  |
      - $A\iff B$ is another way of saying "logically equivalent"
        - This can be used to do three things: 
          - Given A, deduce B
          - Given B, deduce A
          - Given statement A, replace with B
      - $A\implies B$ only allows you to deduce one thing: 
        - Given A, deduce B
    - Inference Rules

    | Inference                                          |       Name | 
    |----------------------------------------------------|------------|
    | $\begin{rcases} p\\q\end{rcases}\implies p\land q$                 | Conjunction|
    | $\begin{rcases} p\\p\rightarrow q\end{rcases}\implies p$           | Modus Ponens|
    | $\begin{rcases} \neg q\\p\rightarrow q\end{rcases}\implies \neg p$ | Modus Tollens|
    | $p\land q \implies p$                                              | Simplification|
    | $p\implies p\lor q$                                                | Addition|

## Proof Sequences
  - A proof sequence is a series of statements and reasons to justify the assertions in the form $A\implies C$
  - The first step is given
    - Subsequently, the proof sequence list the statements $B_1, B_2, B_3,...$
  - Example
    - Write proof for this assertion
      $$
      \begin{rcases}
        p\\
        p\rightarrow q\\
        q\rightarrow r
      \end{rcases}
      \implies r
      $$

      |Step | Statements | Reasons | 
      |-----|-----------------|---------| 
      | 1   | $p$             | given   |
      | 2   |$p \rightarrow q$| given   |
      | 3   |$q \rightarrow r$| given   | 
      | 4   |$q$              | modus ponens, 1,2|
      | 5   | r               | modus ponens, 4,3|
  - Whenever we prove something, we get a new inference rule as a result.
    - Start with the basic inferences, and build bigger ones. 
  - This example was easier to write, but more complicated than a truth table, but it should be similar to geometric proofs. 
  - Another example
    - Prove: 
    $$
    \begin{rcases}
      p \lor q \\
      \neg p
    \end{rcases}
    \implies q
    $$

    |Step | Statements | Reasons | 
    |-----|------------|---------|
    | 1   | $p \lor q$   | given   | 
    | 2   | $\neg p$     | given   |
    | 3   | $\neg \left(\neg p \right) \lor q$ | double negation, 1| 
    | 4   | $\neg p \rightarrow q$ |  simplification, 3| 
    | 5   | $q$ | modus ponens, 4, 2|

## Propositional Logic
  - Earlier we said that "$x$ is even" was not a statement. 
    - This is because the true/false value depends on $x$.
    - However, math works almost exclusively with this kind of formalism. 

### Predicates

#### Predicate
  - A predicate is a declarative sentence where the true/false value depends on one or variables. 
  - We use functions to denote predicates. 
    - Example: 
      - $P\left(x\right)=$ "$x$ is even" 
        - $P\left(6\right)$ is true. 
      - $Q\left(x,y\right)=$ "$x$ is heavier than $y$"
        - $Q\left(``feathers",``bricks"\right)$ is false.
  - The domain is very often implicit. 
    - For example, $P\left(x\right)$ really only makes sense with integers. 
    - $Q\left(x,y,\right)$ only makes sense with physical objects. 
    - Usually you state the domain unless it's overwhelmingly obvious what the domain is going to be. 
  - It turns out that equations are predicates. 
    - Example: 
      - Let $E\left(x\right)$ be $x^2 -x - 6 = 0$ 
        - This equation is essentially an assertion.
        - True or false, a value plugged in for $x$ will equal 0, where you essentially treat $=$ as the verb 
        - $E\left(3\right)$ is true
        - $E\left(4\right)$ is false

### Quantifiers
  - Predicates are not statements by default because they have free variables. 
    - You can convert them to statements by plugging in values for the free variables
    - To do this more generally, you use something called a *quantifier*
  - There are two kinds of quantifiers
    - Universal.
      - This means "for all", and asserts that a predicate will be true for all values of some variable. 
      - This is denoted by $\forall$.
        - A full expression might look like $\left(\forall\right)P\left(x\right)$.
      - Example
        - Let $Z\left(x\right)$ be $x\times 0 = 0$
          - I could easily write something like $\left(\forall x\right)E\left(x\right)$ since all values for x are going to lead to $0$.
    - Existential. 
      - This means that there exists at least one value that satisfies the predicate, though there may be more. 
      - This is denoted by the $\exists$
        - A full expression might look like $\left(\exists x\right)P\left(x\right)$.
      - Example: 
        - Let $E\left(x\right)$ be $x^2-x-6=0$.
        - I could write something to the effect of $\left(\exists x\right) E\left(x\right)$.
          - "There exists some real number such that $x^2-x-6=0$".
          - OR "$x^2 - x - 6 = 0$ has a solution".
  - The power of predicate logic comes when you start combining multiple quantifiers. 
    - Let $N\left(x\right)=$ "$x$ is negative"
    - $\exists x \left(N\left(x\right) \land E\left(x\right)\right)$

### Translation
  - There are lots of ways to write quantifiers in English
    - Examples:
      - Let cars be the domain of $x$. 
      - $P\left(x\right)=$ "$x$ gets good mileage"
      - $Q\left(x\right)=$ "$x$ is large"
      - Let's write this like $\left(\forall x\right)\left(Q\left(x\right)\rightarrow\neg P\left(x\right)\right)$
        - For all cars, if $x$ is large, then $x$ does not get good mileage. 
        - Arguably more natural: "All large cars get bad gas mileage"
        - OR "There aren't any large cars that get good mileage"
          - This implies you could write something like $\left(\nexists x\right)\left(P\left(x\right) \land Q\left(x\right)\right)$
    - Another Example: 
      - Express that the sum for any even number plus an odd number is always odd
        - $P\left(x\right)$ in the domain of integers returns true if the value is even and false if the value is odd. 
        - $\left(\forall x \right) \left(\forall y\right)\left[P\left(x\right) \land \neg P\left(y\right)\right] \rightarrow \left[\neg P\left(x+y\right)\right]$
    - Another Example: 
      - In the domain of all real numbers, $G\left(x,y\right) = x > y$
        - "For all $y$, there exists a bigger $x$"
          - $\left(\forall y\right)\left(\exists x\right) G\left(x,y\right)$
          - This is not true. 
            - There does not exist a number that is bigger than all numbers. 

### Negation
  - In order to do anything particularly useful, we need to know how negations affect quantifiers.
  - Equivalence table
    
    | Equivalence  | Name |
    |--------------|------|
    | $\neg\left[\left(\forall x\right) P \left(x\right)\right] \iff \left(\exists x\right)\left(\neg P\left(x\right)\right)$| universal negation|
    |$\neg\left[\left(\exists x\right)P\left(x\right)\right] \iff \left(\forall x\right) \left(\neg P\left(x\right)\right)$|existential negation|
  
  - Going back to the previous example about cars. 
    - Let's look at the negation of "All large cars get bad mileage"
      - First, let's translate this to a predicate
        - Remember: 
          - $Q\left(x\right)=$ "$x$ is large
          - $P\left(x\right)=$ "$x$ gets good mileage"
        - $\left(\forall x \right) \left( Q\left(x\right) \rightarrow \neg P \left(x\right)\right)$
      - Now let's start the negation process
        
        | Step | Statement | Reasons| 
        |-----|-----------|--------|
        | 1   |  $\neg\left[\left(\forall x\right)\left(Q\left(x\right) \rightarrow \neg P\left(x\right)\right)\right]$  | given|
        | 2   | $\left(\exists x \right) \neg \left(Q\left(x\right) \rightarrow \neg P \left(x\right)\right)$| universal negation|
        | 3   | $\left(\exists x \right) \neg \left(\neg Q\left(x\right) \lor \neg P\left(x\right)\right)$| Implication|
        | 4 | $\left(\exists x \right) \left( \neg \left( \neg Q \left(x\right)\right) \land \neg \left( \neg P \left(x\right)\right) \right)$ | De Morgan's Law | 
        | 5 | $\left(\exists x \right) \left(  Q \left(x\right) \land  P \left(x\right)\right)$ | Double Negation | 
        | 6 | $\left(\exists x \right) \left(  P \left(x\right) \land  Q \left(x\right)\right)$ | Commutativity| 

## Logic in Mathematics.
  - Thus far we have only scratched the surface of mathematical and symbolic logic. 
    - We have still learned enough to loosely reason about mathematical language and see why certain terms are chosen. 
  - We will see certain labels as we move on: 
    - Theorem
    - Definition
    - Proof 

### Role of Definitions in Mathematics.
  - In spoken and written languages, definitions are typically descriptive. 
    - Definitions usually describe specific properties of a thing that already exists. 
  - In math, we do things a bit differently. 
    - By giving a definition, we are *creating* a new term. 
      - Example: 
        - Two lines are parallel if they have no common points.
          - This does not describe a "line", but moreover what we mean by saying "parallel".
          - Not to belabor the point too much, but this *creates* the term as far as we're concerned.
            - Just as a note, I think of this like programming.  If I do something like `let blah = 1 + x`, I am creating a new thing called `blah`, not describing an existing thing. 
      - Another Example. 
        - An integer is even if $n=2k$ for some integer $k$.
        - An integer is odd if $n=2k+1$ for some integer $k$.
  - Mathematical definitions have to be precise to be useful. 
    - It turns out that this can be very limiting, and creates problems in the future. 
    - Example. 
      - We know that a number cannot be both even and odd. 
        - If we take some value $n$ and define it as a value of $2k_1 +1$ and also define it to be as a value $2k_2$, where $k_1$ and $k_2$ may or may not be different. 
        - It follows then that we could write something like: 
          $$
          2k_1 = 2\left(k_2 \right) + 1
          $$
          $$
          1 = 2\left(k_1 - k_2\right)
          $$
        - This then implies that 1 is even, which we know is not the case. 
        - The above definitions are not enough.

### Other Types of Mathematical Statements. 
  - All math has to have assumptions. 
    - We call these "axioms". 
    - Without a starting point, you have nothing to work with. 
        - Example. 
          - Natural Number axiom. 
            - If $n$ is a natural number, so is $n+1$
    - We try and keep our axioms very simple and basic. 
      - Any theorem is based on conclusions drawn from a set of axioms. 
    - A proof is any valid argument based on a set of axioms, definitions, and proven theorems. 
    - A conjecture is a statement that we want to prove. 




### Axiomatic Systems. 
  - In modern mathematics, any systems should be rigorously defined from the start
    - Ideally, nothing should be left up to intuition. 
      - This turns out to be pretty much impossible, since to make things work, we have to bind them to our imprecise written or spoken languages. 
        - Any system we have is going to have undefined terms.
        - We call these kinds of systems "axiomatic systems". 
  - Example: 
    - An axiomatic system for 4 point geometry. 
      - Undefined Terms: point, line, "is on"
      - Axioms
        - For every pair of points $x$ and $y$, there is a unique line $l$ that passes through both $x$ and $y$.
        - Given line $l$ and a point $x$ not on $l$, there is a unique line $m$ such that x is on $m$ and no point on L is also on $m$
        - There are exactly four points. 
        - It is impossible for three points to be on the same line. 
      - This seems pretty precise and unambiguous, but even within this, we have an assumption that natural numbers are already defined by using $3$ and $4$ and making comparisons against them. 
    - "Badda Bing"" Axiomatic System
      - Undefined terms: "badda", "bing", "hits"
      - Axioms
        - Every badda hits exactly 4 bings.
        - Every bing hits exactly two baddas.
        - If $x$ and $y$ are distinct baddas, both hitting bing $q$, then there are no other bings hit by both $x$ and $y$.
        - There is at least one bing. 
      - The important part to take in from this dumb example is to see that the terms "badda" and "bing" mean whatever we want them to mean.  The supplied example is a cool fractal picture, but we could make anything that satisfies the axioms. 
      

### Methods of Proof

#### Direct Proofs
  - Direct Proofs follow the same pattern as our logical proofs of before: 
    - $A \implies B_1 \implies B_2 \implies B_3 ...$
    - The main difference is that the proofs are typically written in a bit more of a prose style than just a logic table.  
  - Example: 
    - Prove the following: For all real values $x$, where $x>1$, $x^2>1$
      - Proof
        - Let $x$ be a real number, and suppose that $x>1$.
        - Since multiplying both sides of an inequality does not change the direction of the comparison, we can multiply both sides of $x>1$ by $x$ and get $x^2>x$.
        - Since $x>1$ was already given, we can now write $x^2>x>1$ which can easily be rewritten $x^2>1$. 
      - Each conclusion from above is justified by an algebraic fact. 
        - The difference, as you can see, was that t was written in paragraph form (or outline in the case of these notes). 
      - Getting down to the guts of it, what we were trying to prove was: 
        - $\left(\forall x\right) \left(P\left(x\right) \rightarrow Q\left(x\right)\right)$.
          - $P\left(x\right)$ would be $x>1$, and $Q\left(x\right)$ would mean $x^2>1$.
  - Another Example: 
    - Before we get into the next example, let us lay out a few more integer rules. 
      - Definition
        - An integer $x$ divides an integer $y$ if there is some integer $k$ such that $y=kx$.
          - We'll use $x | y$ to denote "$x$ divides $y$"
      - Axiom
        - If $a$ and $b$ are integers, so are $a+b$ and $a\times b$.
    - Prove the following: For all integers $a$, $b$, and $c$, if $a | b$ and $a | c$, then $a | \left(b + c\right)$
      - Let integers $a$, $b$, and $c$ be given. 
      - Suppose that $a|b$ and $a|c$
      - By definition, there must be a $b = k_1 a$ and there must also be a $c = k_2a$
      - Therefore
        - $b+c = k_1 a + k_2 a = \left(k_1 + k_2\right) a$
        - By the previous axiom, $k_1 + k_2$ must be an integer, and by the same axiom, $\left(k_1 + k_2\right) a$ must also be an integer. 
        - $\therefore a | \left(b+c\right)$
  - Rule of Thumb for translating logic syntax to English.
    - To prove a statement of the form $\left(\forall x\right) \left(P\left(x\right) \rightarrow Q\left(x\right)\right)$, begin with: 
      - "Let $x$ be [element of domain] and suppose $P\left(x\right)$"


#### Proof by Contradiction. 
  - Proofs can get pretty tricky. 
  - Occasionally it's easier to disprove the opposite of what you're trying to prove. 
    - $\neg A \implies B_1 \implies B_2 \implies ... F$
    - Then, using a contrapositive
      - $A \implies \neg B_1 \implies \neg B_2 \implies ... T$
  - Rule of thumb for translating to English: 
    - To prove statement $A$ by contradiction, begin with: 
      - "Suppose, to the contrary, $\neg A$"
      - Work with this until you get a contradiction. 
  - Example: 
    - In Euclidean Geometry, prove: 
      - If two lines share common perpendiculars, then the lines are perpendicular. 
      - In logic form: $\left(\forall x\right)\left(\forall y\right)\left(C\left(x,y\right)\rightarrow P\left(x,y\right)\right)$
        - $C\left(x,y\right)$ is "$x$ and $y$ share a common perpendicular"
        - $P\left(x,y\right)$ means $x \parallel y$
      - Negation: 
        - $\left(\exists x\right)\left(\exists y\right)\left(C\left(x,y\right) \land \neg P\left(x,y\right)\right)$
          - "There exists lines that share a common perpendicular but are not parallel." 
      - Proof: 
        - Suppose, to the contrary: 
          - Line $AB$ is a common perpendicular between $AC$ and $BD$. 
          - Since it was given that $\neg \left(AC \parallel BD\right)$, then $AC and $BD$ must intersect at some point $x$. 
          - Problem: If the lines intersect, this would effectively create a triangle with sides $AB$, $AC$, and $BD$.  Since $AB$ is a common *perpendicular*, that means that we would have two $90^\circ$ angles.  
            - Therefore any angle between $AC$ and $BD$ would put the internal angles at more than $180^\circ$. 
              - You cannot have a triangle with more than $180^\circ$ of internal angles. 
              - $\therefore$ $AC$ and $BD$ are parallel. 
  - Another Example: 
    - Before we start, remember the definitions for even and odd numbers: 
      - Odd can be defined as some integer $n$ where $n=2k+1$ with some integer $k$.
      - Even can be defined as some integer $n$ where $n=2k$ with some integer $k$.
      - Axiom
        - For all values integers $n$, $\neg\left("n_is_even" \iff "n_is_odd"\right)$
      - Lemma: Let $n$ by even, then $n^2$ is even. 
      - Proof by contradiction: 
        - Let $n$ be an integer, and suppose $n$ is not even. 
        - $\therefore$ $n$ is odd. 
        - $\therefore$ there is some integer $k$ where $n=2n+1$.
        - $n^2 = \left(2k+1\right)^2 = 4k^2 + 4k + 1 = 2\left(2k^2 + 2\right) + 1$. 
        - $2k^2 + k$ is an integer. 
        - $\therefore$ $2\left(2k^2 + k\right)$ is even.
        - $\therefore$ $2\left(2k^2 + k\right) + 1$ is odd.
        - $n^2$ is not even.
  - Another Example: 
    - Prove that $\sqrt 2$ is irrational.
      - Proof by contradiction: 
        - Suppose, to the contrary, $\sqrt 2$ is rational. 
        - This means that there is some $\frac a b = \sqrt 2$ where $a$ and $b$ are in their lowest term.
        - This means $\frac {a^2} {b^2} = 2$.
        - This means that $a^2 = 2b^2$.
        - $a^2$ must be even.
        - $\therefore$ $a=2k$ in some sense.
        - $\therefore$ $b^2=\frac {a^2} {2k^2}$
        - $\therefore$ $b^2$ must be even.
        - Since we stated that $a$ and $b$ are in their simplest terms, and both numbers are even, this is a contractdiction. 
        - $\therefore$ $\frac a b = \sqrt 2$ is a contradiction and $\sqrt 2$ is irrational.

# Chapter 2

## Graphs
- It is sometimes better to draw a picture of to understand the entirety of a problem. 
  - If there are lots of interrelated objects, a natural way to represent them is to draw the objects, and draw lines between them to form a "connection".
- Graph
  - A graph is a mathematical version of this kind of sketch.
  - Graph theory can get really big and complex, so we're going to start a bit more informally. 

## Edges and Vertices
  - Right now, it's best to think of a graph as a diagram of dots. 
    - We call these dots "vertices".  
  - Lines connecting these vertices are called "edges".
    - Edges might have arrows.
      - When the edges have arrows, this is called a directed graph. 
      - When the edges do not have arrows, this is called (unsurprisingly) an undirected graph.
  - When drawing a graph, it does not particularly matter *where* you draw the vertices or edges, or if the edges are straight or curvy. 
    - What matters is how the vertices and edges are actually connected.
  - Example
    - The Pregel River is divided into four sections. 
      - It is connected together by bridges. 
      - I'm not going to draw the picture of the bridge, but I'll draw the diagram.

        \begin{tikzpicture}
        \tikzset{VertexStyle/.style = {shape          = circle,
                ball color     = black,
                text           = white,
                inner sep      = 2pt,
                outer sep      = 0pt,
                minimum size   = 24 pt}}
        \tikzset{EdgeStyle/.style   = {thick,
                double          = black,
                double distance = 1pt}}
        \tikzset{LabelStyle/.style =   {draw,
                fill           = yellow,
                text           = red}}
        \node[VertexStyle](A){A};
        \node[VertexStyle,right=of A](B){B};
        \node[VertexStyle,right=of B](C){C};
        \node[VertexStyle,above= of B](D){D};     
        \draw[EdgeStyle](B) to node{} (D) ;
        \tikzset{EdgeStyle/.append style = {bend left}}
        \draw[EdgeStyle](A) to node{} (B);
        \draw[EdgeStyle](B) to node{} (A);
        \draw[EdgeStyle](B) to node{} (C);
        \draw[EdgeStyle](C) to node{} (B);
        \draw[EdgeStyle](A) to node{} (D);
        \draw[EdgeStyle](D) to node{} (C);

        \end{tikzpicture}

      - For some reason, my image won't align correctly. 
      - Euler wanted to know if there was a path you could take where you touched all the bridges only once. 

## Terminology
  - The Degree of a Vertex.
    - This is the number of times an edge touches it.
  - Indegree
    - The number of edges coming out of a vertex. 
  - Outdegree
    - The number of edges going out. 
  - Path
    - $v_0,e_1,v_1,e_2,v_2,...,v_{n-1},e_n,v_n$
  - Circuit
    - A circuit is a path that ends where it began. 
  - Undirected Graph
    - A graph is undirected if there is a path to connect any two vertices. 
  - Directed graph is connected if the underlying undirected graph is connected.
  - Why do we need all these terms? 
    - These terms make it easier to make very precise description.

## Back to the Bridges. 
  - The number of bridges written next to letters $A,B,C,...$ together add up to twice the total bridges.
    - The reason for this is because in the calculation where every bridge leading to an area is counted, each bridge is counted twice.
      - Once for each of the two areas it joins. 
  - If there are more than two areas to which an odd number of bridges lead, the "touch only once" thing is impossible.
  - If the number of bridges is odd for exactly two areas, provided that you start from one of those areas. 
  - If no area has an odd number of of bridges, you start from anywhere. 



